<?php
use Migrations\AbstractSeed;

/**
 * Articles seed.
 */
class ArticlesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'ねこねこへいへい',
                'body' => 'この黒猫はかわゆすだよ〜。',
                'created' => date('Y-m-d H:i:s'),
                'modified' => '2009-12-01 00:00:00',
                'img' => '/img/82.jpg'
            ],
            [
                'title' => '黒猫じじ',
                'body' => 'にゃーにゃー',
                'created' => date('Y-m-d H:i:s'),
                'modified' => '2009-12-01 00:00:00',
                'img' => '/img/S9e-TuTg.jpeg'
            ],
            [
                'title' => 'ねこちゃん！',
                'body' => 'このねこかわいいぜい',
                'created' => date('Y-m-d H:i:s'),
                'modified' => '2009-12-01 00:00:00',
                'img' => '/img/lgf01a201408062000.jpg'
            ]
        ];

        $table = $this->table('articles');
        $table->insert($data)->save();
    }
}
