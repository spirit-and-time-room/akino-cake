<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class TopsController extends AppController
{
    public function index()
    {
        $this->Articles = TableRegistry::get('articles');

        $articles = $this->Articles->find('all')->all();
        $data = $articles->toArray();

        $this->set('articles', $data);
    }
}
