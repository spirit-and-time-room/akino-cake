<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="articles form large-9 medium-8 columns content">
    <?= $this->Form->create($document, ['type' => 'file']); ?>
    <fieldset>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('body');
            echo $this->Form->file('submittedfile');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
