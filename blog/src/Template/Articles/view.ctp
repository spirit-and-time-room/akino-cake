<main>
    <div class="view-article clearfix">
        <div class="view-article__image">
            <img src="<?= $article->img ?>" alt="view-article__image">
        </div>
        <div class="view-article__information">
            <div class="view-article__title">
                <?= $article->title ?>
            </div>
            <div class="view-article__body">
                <?= $article->body ?>
            </div>
        </div>
    </div>
</main>
