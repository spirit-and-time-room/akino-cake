<main>
    <ul class="articles clearfix">
        <?php foreach ($articles as $article): ?>
        <li class="articles__item">
            <a href="<?php echo $this->Url->build(["controller" => "Articles","action" => "view",$article->id]); ?>">
                <div class="articles__item__thumbnail">
                    <img src="<?= $article->img ?>" alt="articles__item__thumbnail">
                </div>
                <div class="articles__item__information">
                    <div class="article__item__title">
                        <p><?= $article->title ?></p>
                    </div>
                    <div class="article__item__read">
                        <?= $article->body ?>
                    </div>
                </div>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</main>
